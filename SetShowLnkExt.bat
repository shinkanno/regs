@Echo Off
Title Reg Converter v1.2 & Color 1A
cd %systemroot%\system32
call :IsAdmin

reg delete "HKCR\InternetShortcut"     /v NeverShowExt /f
reg add    "HKCR\InternetShortcut"     /v AlwaysShowExt /t REG_SZ /f
reg delete "HKCR\IE.AssocFile.URL"     /v NeverShowExt /f
reg add    "HKCR\IE.AssocFile.URL"     /v AlwaysShowExt /t REG_SZ /f
reg delete "HKCR\lnkfile"              /v NeverShowExt /f
reg add    "HKCR\lnkfile"              /v AlwaysShowExt /t REG_SZ /f
reg delete "HKCR\piffile"              /v NeverShowExt /f
reg add    "HKCR\piffile"              /v AlwaysShowExt /t REG_SZ /f
reg delete "HKCR\Microsoft.Website"    /v NeverShowExt /f
reg add    "HKCR\Microsoft.Website"    /v AlwaysShowExt /t REG_SZ /f
reg delete "HKCR\IE.AssocFile.WEBSITE" /v NeverShowExt /f
reg add    "HKCR\IE.AssocFile.WEBSITE" /v AlwaysShowExt /t REG_SZ /f
Pause
Exit

:IsAdmin
Reg.exe query "HKU\S-1-5-19\Environment"
If Not %ERRORLEVEL% EQU 0 (
 Cls & Echo 管理者として実行してください ... 
 Pause & Exit
)
Cls
goto:eof
